<?php

class MediaModel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function getRoyalData()
    {
        $dataArray = array();
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201312.png'),base_url('cms-storage/sawasdee/files/201312')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201408.png'),base_url('cms-storage/sawasdee/files/201408')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201407.png'),base_url('cms-storage/sawasdee/files/201407')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201404.png'),base_url('cms-storage/sawasdee/files/201404')));
        //array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201308.png'),base_url('cms-storage/sawasdee/files/201308')));
        //array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201307.png'),base_url('cms-storage/sawasdee/files/201307')));
        //array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201304.png'),base_url('cms-storage/sawasdee/files/201304')));
        //array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201212.png'),base_url('cms-storage/sawasdee/files/201212')));
        //array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201208.png'),base_url('cms-storage/sawasdee/files/201208')));
        //array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201207.png'),base_url('cms-storage/sawasdee/files/201207')));
        return $dataArray;
    }


    public function getFortnightlyData()
    {
        $dataArray = array();
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201406.png'),base_url('cms-storage/sawasdee/files/201406')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201405.png'),base_url('cms-storage/sawasdee/files/201405')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201403.png'),base_url('cms-storage/sawasdee/files/201403')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201402.png'),base_url('cms-storage/sawasdee/files/201402')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201401.png'),base_url('cms-storage/sawasdee/files/201401')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201311.png'),base_url('cms-storage/sawasdee/files/201311')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201310.png'),base_url('cms-storage/sawasdee/files/201310')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201309.png'),base_url('cms-storage/sawasdee/files/201309')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201306.png'),base_url('cms-storage/sawasdee/files/201306')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201305.png'),base_url('cms-storage/sawasdee/files/201305')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201306.png'),base_url('cms-storage/sawasdee/files/201306')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201303.png'),base_url('cms-storage/sawasdee/files/201303')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201302.png'),base_url('cms-storage/sawasdee/files/201302')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201301.png'),base_url('cms-storage/sawasdee/files/201301')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201211.png'),base_url('cms-storage/sawasdee/files/201211')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201210.png'),base_url('cms-storage/sawasdee/files/201210')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201209.png'),base_url('cms-storage/sawasdee/files/201209')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201206.png'),base_url('cms-storage/sawasdee/files/201406')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201205.png'),base_url('cms-storage/sawasdee/files/201205')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201204.png'),base_url('cms-storage/sawasdee/files/201204')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201203.png'),base_url('cms-storage/sawasdee/files/201203')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201202.png'),base_url('cms-storage/sawasdee/files/201202')));
        array_push($dataArray,array(base_url('assets/images/media/sawasdee/cover/201201.png'),base_url('cms-storage/sawasdee/files/201201')));
        return $dataArray;
    }
}