<?php /*?>
   <div class="row beforeyourfly-cover focuspoint"  data-focus-x="0" data-focus-y="0.6">
    <img src="<?php echo base_url('assets/images/before/detail/cover.jpg');?>" alt="" class="img-responsive"/>
</div>
<?php */?>
<div class="row beforeyourfly-detail">
    <div class="container">
        <div class="row beforeyourfly-breadcrumb">
            <a href="<?php echo site_url('beforeyourfly.html');?>" class="back">< Back</a> 
                <span> Before Your Fly / Pet Travel</span>
                </div>

            <div class="row article">
                <div class="col-sm-12">
                    <h1>Pet Travel</h1>
                    <h2 class="general">General service</h2>
                    <section>
                        <p>Transporting pets can be a stressful experience for both the passenger and their beloved pet. Therefore, THAI is committed to providing caring, 
                            comfortable and stress-free service for both.</p>
                        <p>
                            Advanced arrangements for pet travel services must be made at time of booking, at least 48 hours prior to flight departure.  Passengers are required to 
                            alert their travel agent or local THAI office at the time of booking that they will be traveling with their pet. THAI’s staff will check on space availability 
                            and provide advice on regulations for the travel of pets to final destination and/or transit points.</p>
                    </section>
                    <section>
                        <img src="<?php echo base_url('assets/images/before/detail/thumb-1.png');?>" alt="" class="img-responsive pull-left">

                        <p>Valid export and import permits from the relevant animal quarantine officer Valid health and rabies vaccination certificate 
                            from the authorizing veterinarian The Animal Quarantine Bureau of Japan must be notified at least 40 days in advance of 
                            scheduled arrival of dogs and cats that will enter Japan. The Bureau will issue a document known as ‘Approval of Inspection
                            of Animal’. Prior to entering India, a –No Objection Certificate- (NOC) must be issued locally by the Animal Quarantine and
                            Certification Services (AQCS).</p>

                        <p>
                            Dogs and cats entering EU communities must be implanted with Microchip. Some countries, e.g. UK, Australia, New Zealand,
                            United Arab Emirates, Hong Kong (SAR China), have imposed regulations to allow pets into their countries as cargo only.
                            Please refer to the topic –Cargo-.THAI strongly recommends that passengers obtain details regarding documents required 
                            for pet transportation from the relevant  embassy website or your nearest embassy.There are a limited number of pets 
                            allowed, due to rules and regulations of each country. Some breeds of dogs, e.g.  pitbull terrier dogs, are prohibited into 
                            some countries, such as Thailand.</p>

                    </section>
                    <section>
                        <img src="<?php echo base_url('assets/images/before/detail/thumb-2.png');?>" alt="" class="img-responsive pull-right">
                        <p>Valid export and import permits from the relevant animal quarantine officer Valid health and rabies vaccination certificate 
                            from the authorizing veterinarian The Animal Quarantine Bureau of Japan must be notified at least 40 days in advance of 
                            scheduled arrival of dogs and cats that will enter Japan. The Bureau will issue a document known as ‘Approval of Inspection
                            of Animal’. Prior to entering India, a –No Objection Certificate- (NOC) must be issued locally by the Animal Quarantine and
                            Certification Services (AQCS).</p>

                        <p>
                            Dogs and cats entering EU communities must be implanted with Microchip. Some countries, e.g. UK, Australia, New Zealand,
                            United Arab Emirates, Hong Kong (SAR China), have imposed regulations to allow pets into their countries as cargo only.
                            Please refer to the topic –Cargo-.THAI strongly recommends that passengers obtain details regarding documents required 
                            for pet transportation from the relevant  embassy website or your nearest embassy.There are a limited number of pets 
                            allowed, due to rules and regulations of each country. Some breeds of dogs, e.g.  pitbull terrier dogs, are prohibited into 
                            some countries, such as Thailand.</p>

                    </section>
                </div>
            </div>
        </div>
    </div>