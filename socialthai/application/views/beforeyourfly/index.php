<div class="row beforeyourfly-content">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>Before Your Fly</h1>
            </div>
        </div>
        <?php /*?>
        <div class="row form-dropdown">
            <div class="col-sm-2 col-sm-offset-10">
                <form class="form-horizontal" role="form">
                    <select class="form-control">
                        <option>Show All</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </form>
            </div>
        </div>
        <?php */?>
        <div class="row before-list">
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-1.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">General Service</div><?php */?> 
                <div class="caption general">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Air Travel with Children</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 4 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-2.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">Travel Information</div><?php */?> 
                <div class="caption travel">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Pet Travel</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 2 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-3.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">Special Service</div><?php */?>
                <div class="caption special">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Expectant Mothers</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 4 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-4.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">Travel Information</div><?php */?>
                <div class="caption travel">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Wheelchair Requests</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 2 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-5.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">General Service</div><?php */?>
                <div class="caption general">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Advice for elderly passengers</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 1 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-6.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">General Service</div><?php */?>
                <div class="caption general">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Insecticide use on Aircraft</p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 1 hrs.  Thu, 17 Nov 2014</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-7.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">Special Service</div><?php */?>
                <div class="caption special">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Traveling with Infants</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 4 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-8.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">Special Service</div><?php */?>
                <div class="caption special">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Special Inflight Meals</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 4 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 before-group">
                <div class="focuspoint">
                    <a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>" class="img-over">
                        <img src="<?php echo base_url('assets/images/before/thumb-9.jpg')?>" alt="" class="img-responsive">
                    </a>
                </div>
                <?php /*?><div class="title">General Service</div><?php */?>
                <div class="caption general">
                    <p class="topic"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Nut Allergy</a></p>
                    <p class="date"><a href="<?php echo site_url('beforeyourfly/general-service/pet-travel.html');?>">Update : 1 hrs.  Thu, 17 Nov 2014</a></p>
                </div>
            </div>
        </div>
    </div>
</div>