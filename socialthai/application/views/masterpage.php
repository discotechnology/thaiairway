<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php //echo $seoTitle;?></title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
        <!-- Style -->
        <link href="<?php echo base_url('assets/fonts/webfontkit/stylesheet.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/libs/focuspoint/css/focuspoint.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/libs/bxslider/jquery.bxslider.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/jquery.selectbox.css');?>" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="<?php echo base_url('assets/js/html5shiv.js');?>"></script>
<script src="<?php echo base_url('assets/js/respond.min.js');?>"></script>
<![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.js');?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.selectbox-0.2.min.js');?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    </head>
    <body>
        <div class="container-fluid">
            <header>
                <div class="row social-group">
                    <div class="col-xs-12">
                        <ul class="pull-right">
                            <li>Follow Us</li>
                            <li><a href="https://www.facebook.com/ThaiAirways" target="_blank" class="fb-btn">Facebook</a></li>
                            <li><a href="https://twitter.com/ThaiAirways/" target="_blank" class="tw-btn">Twitter</a></li>
                            <li><a href="http://www.youtube.com/user/ThaiAirwaysChannel" target="_blank" class="yt-btn">Youtube</a></li>
                            <li><a href="http://www.thaiairways.com/th_TH/news/news_announcement/news_detail/line.page?" target="_blank" class="line-btn">Line</a></li>
                            <li><a href="http://instagram.com/ThaiAirways/#" target="_blank" class="ig-btn">Instagram</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row  nav-group">
                    <div class="col-sm-2 col-md-2 hidden-xs">
                        <a href="<?php echo site_url();?>" class="logo">
                            <img src="<?php echo base_url('assets/images/header/logo.png');?>" alt="" class="img-responsive"/>
                        </a>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand visible-xs" href="#" ><img src="<?php echo base_url('assets/images/header/logo.png');?>" alt="" class="img-responsive"/></a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        <li><a href="<?php echo site_url('home.html');?>">Home</a></li>
                                        <li><a href="<?php echo site_url('beforeyourfly.html');?>">Before Your Fly</a></li>
                                        <li class="dropdown shareexperience">
                                            <a href="<?php echo site_url('shareexperience');?>" class="dropdown-toggle" data-toggle="dropdown">
                                                Share Experience <span>
                                                <img src="<?php echo base_url('assets/images/header/icon-dropdown.png');?>" alt="" />
                                                </span>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo site_url('shareexperience/domestic.html');?>">ภายในประเทศ</a></li>
                                                <li><a href="<?php echo site_url('shareexperience/international.html');?>">ต่างประเทศ</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown mediainformation"><a href="<?php echo site_url('mediainformation');?>" class="dropdown-toggle" data-toggle="dropdown">Media & Informatoin 
                                            <span><img src="<?php echo base_url('assets/images/header/icon-dropdown.png');?>" alt="" /></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo site_url('media/entertainment.html');?>">Entertainment Onboard</a></li>
                                                <li><a href="<?php echo site_url('media/sawasdee.html');?>">Sawasdee Magazine</a></li>
                                                <li><a href="http://issuu.com/kingpowergroup/docs/in-flight-duty-free-thaismile-augus" target="_blank">Duty Free (Thai Smile)</a></li>
                                                <li><a href="http://issuu.com/kingpowergroup/docs/in-flight-duty-free-thaiairways-aug" target="_blank">Duty Free (ThaiAirWay)</a></li>
                                                <li><a href="<?php echo site_url('media/a380.html');?>">A380</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown contactus last">
                                            <a href="<?php echo site_url('contactus');?>" class="dropdown-toggle" data-toggle="dropdown">Contact Us <span><img src="<?php echo base_url('assets/images/header/icon-dropdown.png');?>" alt="" /></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo site_url('contactus/socialteam.html');?>">Social Team</a></li>
                                                <li><a href="<?php echo site_url('contactus/thaiworldwideoffice.html');?>">Thai World Wide Office</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </header>
            <main>
                <?php echo $content;?>
            </main>
            <footer>
                <div class="row footer-sitemap">
                    <div class="container">
                        <div class="col-sm-3">
                            <a href="#" class="logo"><img src="<?php echo base_url('assets/images/footer/logo.png');?>" alt="" class="img-responsive"/></a>
                        </div>
                        <div class="col-sm-9">
                            <ul class="clearfix">
                                <li><a href="#">Thaiairways</a></li>
                                <li><a href="#">Investor</a></li>
                                <li><a href="#">Relations</a></li>
                                <li><a href="#">Corporate</a></li>
                                <li><a href="#">Communications</a></li>
                                <li><a href="#">Procurement</a></li>
                                <li><a href="#">Sitemap</a></li>
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                            <p class="copyright">© 2011 THAI AIRWAYS. All rights reserved. Flash Player Required. <a href="#">Click here to Download</a></p>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="row footer-special">
                <div class="col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="#" class="pull-right btn-open-footer"><img src="<?php echo base_url('assets/images/footer/btn-top.jpg');?>" alt="" /></a>
                                <div class="row footer-special-inner">
                                    <div class="col-sm-3 col-xs-12">
                                        <center><img src="<?php echo base_url('assets/images/footer/qr.png');?>" alt="" class="img-responsive"></center>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <ul class="bxslider" id="bxslider-1">
                                            <li><img src="<?php echo base_url('assets/images/footer/slider/thumb-1.png');?>" /></li>
                                            <li><img src="<?php echo base_url('assets/images/footer/slider/thumb-2.png');?>" /></li>
                                            <li><img src="<?php echo base_url('assets/images/footer/slider/thumb-1.png');?>" /></li>
                                            <li><img src="<?php echo base_url('assets/images/footer/slider/thumb-2.png');?>" /></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2 col-xs-5 print">
                                        <span>
                                            <img src="<?php echo base_url('assets/images/footer/icon-printer.png');?>" alt="" class="img-responsive pull-left disable">
                                            <img src="<?php echo base_url('assets/images/footer/icon-download.png');?>" alt="" class="img-responsive pull-left disable">
                                        </span>
                                    </div>
                                    <div class="col-sm-2 share col-xs-5">
                                        <span>
                                            <img src="<?php echo base_url('assets/images/footer/share.png');?>" alt="" class="img-responsive pull-left hidden-sm hidden-xs">
                                            <img src="<?php echo base_url('assets/images/footer/icon-fb.png');?>" alt="" class="img-responsive pull-left">
                                            <img src="<?php echo base_url('assets/images/footer/icon-tw.png');?>" alt="" class="img-responsive pull-left">
                                        </span>
                                    </div>
                                    <div class="col-sm-1 col-xs-2 totop">
                                        <span><img src="<?php echo base_url('assets/images/footer/totop.png');?>" alt="" class="img-responsive"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- libs -->
        <script src="<?php echo base_url('assets/libs/focuspoint/js/jquery.focuspoint.js');?>"></script>
        <script src="<?php echo base_url('assets/libs/bxslider/jquery.bxslider.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/main.js');?>"></script>
        <script>
            $(function(){
                equalheight = function(container){

                    var currentTallest = 0,
                        currentRowStart = 0,
                        rowDivs = new Array(),
                        $el,
                        topPosition = 0;
                    $(container).each(function() {

                        $el = $(this);
                        $($el).height('auto')
                        topPostion = $el.position().top;

                        if (currentRowStart != topPostion) {
                            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                                rowDivs[currentDiv].height(currentTallest);
                            }
                            rowDivs.length = 0; // empty the array
                            currentRowStart = topPostion;
                            currentTallest = $el.height();
                            rowDivs.push($el);
                        } else {
                            rowDivs.push($el);
                            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                        }
                        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                            rowDivs[currentDiv].height(currentTallest);
                        }
                    });
                }
                $(window).load(function() {
                    equalheight('.shareexperience-lists .lists .intro');
                    equalheight('.home-content .box-content');
                });

                $(window).resize(function(){
                    equalheight('.shareexperience-lists .lists .intro');
                    equalheight('.home-content .box-content');
                });

                var footerSpecialInnerHeight = $('.footer-special > div').height();
                $('.footer-sitemap').height($('.footer-special .container').height() - 30);

                $( window ).resize(function() {
                    $('.footer-sitemap').height($('.footer-special .container').height() - 30);
                    footerSpecialInnerHeight = $('.footer-special > div').height();
                    if(!isShowFooterSpecial){
                        $('.footer-special > div').css("bottom",-footerSpecialInnerHeight);
                    }
                });
                var isShowFooterSpecial = false;
                showFooterSpacial(isShowFooterSpecial);
                $('.btn-open-footer').click(function(){
                    isShowFooterSpecial = !isShowFooterSpecial;
                    showFooterSpacial(isShowFooterSpecial);
                    return false;
                });

                function showFooterSpacial(isShowFooterSpecial){
                    $('.footer-special > div').stop(true).animate({bottom: (isShowFooterSpecial)?-3:-footerSpecialInnerHeight},100);
                    if(isShowFooterSpecial){
                        $('.btn-open-footer').addClass('active');
                    }else{
                        $('.btn-open-footer').removeClass('active');
                    }
                }

                $('.shareexperience-content .list-group>div').hover(
                    function() {
                        $('.mask', this).stop().fadeTo("fast", 0);
                    }, function() {
                        $('.mask', this).stop().fadeTo("fast", 0.5);
                    }
                );

                $('.focuspoint').focusPoint();
                $('#bxslider-1').bxSlider({
                    minSlides: 1,
                    maxSlides: 2,
                    slideWidth: 150,
                    slideMargin: 10,
                    pager: false
                });
            });
        </script>
    </body>
</html>
