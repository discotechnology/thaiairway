<?php /*?>
<div class="row socialteam-cover focuspoint"  data-focus-x="0" data-focus-y="0.6">
    <img src="<?php echo base_url('assets/images/contactus/socialteam/cover.jpg');?>" alt="" class="img-responsive"/>
</div>
<?php */?>
<div class="row socialteam-detail">
    <div class="container">
        <div class="row socialteam-breadcrumb">
            <a href="<?php echo site_url('beforeyourfly');?>" class="back">< Back</a> 
                <span> Contact Us / Social Team</span>
                </div>
            <div class="row article">
                <div class="col-sm-6">
                    <h1>Social Team</h1>
                    <p>
                        Corparate Communication Department<br/>
                        Thai Airways International Public Company Limited<br/>
                        89 Vibhavadi Rangsit Road<br/>
                        Bangkok 10900, Thailand<br/>
                        Tel : (662) 545-1000<br/>
                        Fax.(662) 545 3894-2<br/>
                    </p>
                </div>
                <div class="col-sm-6">
                    <img src="<?php echo base_url('assets/images/contactus/socialteam/map.jpg');?>" alt="" class="img-responsive">
                        <br/><br/><br/>
                </div>
            </div>
        </div>
    </div>