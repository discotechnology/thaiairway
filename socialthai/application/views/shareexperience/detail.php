<div class="row shareexperience-cover focuspoint" data-focus-x="0" data-focus-y="0.35">
    <img src="<?php echo base_url('assets/images/share/cover.jpg');?>" alt="" />
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <h1>HONG KONG</h1>
                <h2>Hong Kong</h2>
            </div>
        </div>
    </div>
</div>
<div class="row shareexperience-detail">
    <div class="container">
        <div class="row beforeyourfly-breadcrumb"><a href="<?php echo site_url('shareexperience/'.$type.'/'.$country.'.html');?>" class="back">< Back</a> <span> Before Your Fly / Pet Travel</span></div>
            <div class="row author-panel">
                <div class="col-sm-12">
                    <div class="author">
                        <img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>Masaru Goto</b> | <span class="date">21 Apr 14</span>
                        <br/><sub class="edit"><a href="">edit</a></sub>
                    </div>
                    <div class="intro">
                        <h3>Dabble in Hongkong Part 1</h3>
                        <p>One of Osaka's wealthiest destinations, Aoyama is not the wildly experimental and wily playful fashion district of Harajuku just to its west.</p>
                    </div>
                </div>
            </div>
            <div class="row article">
                <div class="col-sm-12">
                    <h1>High Fashion and Fast Pace in the Heart of Hong Kong</h1>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="focuspoint">
                                <img src="<?php echo base_url('assets/images/share/detail/mockup-1.jpg');?>" alt="">
                            </div>
                            <p>A few miles south of the world's busiest train station and just west of its most imaginatively-dressed
                                neighborhood, Harajuku, Aoyama elegantly blends fast-paced lifestyles and chic fashion conscientiousness.</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="focuspoint">
                                <img src="<?php echo base_url('assets/images/share/detail/mockup-2.jpg');?>" alt="">
                            </div>
                            <p>Near Osaka's core, Roppongi took root when the Japanese army moved in during the late 19th century.</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="focuspoint">
                                        <img src="<?php echo base_url('assets/images/share/detail/mockup-3.jpg');?>" alt="">
                                    </div></div>
                                <div class="col-xs-6">
                                    <div class="focuspoint">
                                        <img src="<?php echo base_url('assets/images/share/detail/mockup-4.jpg');?>" alt="">
                                    </div>
                                </div>
                            </div>
                            <p>The neighborhood really blossomed when the United States built its main military base in
                                Roppongi's neighboring district after WWII.</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="focuspoint">
                                <img src="<?php echo base_url('assets/images/share/detail/mockup-5.jpg');?>" alt="">
                            </div>
                            <p>Roppongi Hills and Tokyo Midtown's offices and shopping complexes soar above the neighborhood's previous dwellings.</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="focuspoint">
                                        <img src="<?php echo base_url('assets/images/share/detail/mockup-6.jpg');?>" alt="">
                                    </div></div>
                                <div class="col-xs-6">
                                    <div class="focuspoint">
                                        <img src="<?php echo base_url('assets/images/share/detail/mockup-7.jpg');?>" alt="">
                                    </div>
                                </div>
                            </div>
                            <p>From upscale restaurants to street wise kebab trucks, daytime professionals and hungry night-owls find satisfies.</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="focuspoint">
                                <img src="<?php echo base_url('assets/images/share/detail/mockup-8.jpg');?>" alt="">
                            </div>
                            <p>Open air cafes and interactive museums offer wholesome alternatives to the neighborhood's bars and clubs.</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="focuspoint">
                                <img src="<?php echo base_url('assets/images/share/detail/mockup-9.jpg');?>" alt="">
                            </div>
                            <p>American-inspired grills and Italian-influenced street food stalls often stay open as long as Roppongi's partiers stay up.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>