<div class="row shareexperience-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-10 col-lg-offset-1">
                <h1>Domestic</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row list-group">
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/share-1.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">THAI</p>
                            <p class="province">Chiang Mai</p>
                            <p class="number">7</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/domestic/chiangmai.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/share-2.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">THAI</p>
                            <p class="province">Phuket</p>
                            <p class="number">10</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/domestic/phuket.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/share-3.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">THAI</p>
                            <p class="province">Bangkok</p>
                            <p class="number">9</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/domestic/bangkok.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/share-4.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">THAI</p>
                            <p class="province">Buriram</p>
                            <p class="number">5</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/domestic/buriram.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>