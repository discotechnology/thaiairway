<div class="row shareexperience-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-10 col-lg-offset-1">
                <h1>International</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row list-group">
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/international/share-1.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">Spain</p>
                            <p class="province">Madrid</p>
                            <p class="number">10</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/international/madrid.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/international/share-2.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">KOREA</p>
                            <p class="province">Seoul</p>
                            <p class="number">6</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/international/seoul.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/international/share-3.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">CHINA</p>
                            <p class="province">Shanghai</p>
                            <p class="number">9</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/international/shanghai.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3">
                        <img src="<?php echo base_url('assets/images/share/international/share-4.jpg');?>" alt="" class="img-responsive">
                        <div class="mask"></div>
                        <div class="list-content">
                            <p class="country">HONGKONG</p>
                            <p class="province">Hong Kong</p>
                            <p class="number">12</p>
                            <p class="review">Review</p>
                            <a href="<?php echo site_url('shareexperience/international/hongkong.html');?>" class="readmore">READ MORE ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>