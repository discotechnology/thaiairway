<div class="row shareexperience-cover focuspoint" data-focus-x="0" data-focus-y="0.35">
    <img src="<?php echo base_url('assets/images/share/cover.jpg');?>" alt="" />
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <h1>HONG KONG</h1>
                <h2>Hong Kong</h2>
            </div>
        </div>
    </div>
</div>
<div class="row shareexperience-lists">
    <div class="container">
        <div class="row beforeyourfly-breadcrumb">
            <p><a href="<?php echo site_url('shareexperience/'.$type.'.html');?>" class="back">< Back</a> <span> Share Experience / <?php echo $name;?></span></p>
        </div>

        <div class="row header-group">
            <div class="col-sm-12">
                <h3 class="">12 Review match</h3>
                <h2>At the first visit to <?php echo $name;?> with a giant</h2>
            </div>
        </div>
        <div class="row">
            <?php
if($country == 'hongkong'){
    for($i = 1;$i <=12 ;$i++){            
            ?>
            <div class="lists col-md-4 col-sm-6">
                <div class="thumb focuspoint">
                    <img src="<?php echo base_url('assets/images/media/hongkong/HysanPlace-03.jpg');?>" alt="" />
                </div>
                <div class="author">
                    <img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>Masaru Goto</b> <span class="date">30 Nov 13</span>
                </div>
                <div class="intro">
                    <h3>The Never Ending Story of Hong Kong</h3>
                    <p>ฮ่องกง เป็นเมืองที่ไม่เคยหยุดนิ่ง และมักจะมีอะไรแปลกใหม่ให้นักท่องเที่ยวได้ตื่นเต้นทุกครั้งเมื่อไปสัมผัส... 
                        <a href="http://social.thaiairways.com/share-experience/international/hongkong/MasaruGoto/301113.html">READ MORE ></a>
                    </p>
                </div>
            </div>
            <?php 
    }
}else if($country == 'madrid'){
    for($i = 1;$i <=12 ;$i++){
            ?>
            <div class="lists col-md-4 col-sm-6">
                <div class="thumb focuspoint">
                    <img src="<?php echo base_url('assets/images/media/madrid/PlazaMayor-01.jpg');?>" alt="" />
                </div>
                <div class="author">
                    <img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>David billa</b> <span class="date">30 Jan 13</span>
                </div>
                <div class="intro">
                    <h3>Wandering in Fusion Madrid</h3>
                    <p>สเปนอ้าแขนเปิดรับนักท่องเที่ยวจากทั่วโลกด้วยหัวใจที่อบอุ่น ความหลากหลายทางวัฒนธรรมหล่อหลอมให้สเปนมีเสน่ห์น่าลุ่มหลง... 
                        <a href="http://social.thaiairways.com/share-experience/international/madrid/Davidbilla/300113.html">READ MORE ></a>
                    </p>
                </div>
            </div>
            <?php
    }
}else if($country == 'seoul'){
    for($i = 1;$i <=12 ;$i++){
            ?>
            <div class="lists col-md-4 col-sm-6">
                <div class="thumb focuspoint">
                    <img src="<?php echo base_url('assets/images/media/seoul/BukchonHanokVlg-02.jpg');?>" alt="" />
                </div>
                <div class="author">
                    <img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>Kim Leehun</b> <span class="date">12 Feb 13</span>
                </div>
                <div class="intro">
                    <h3>See your Soul Through Seoul</h3>
                    <p>หากพูดถึงหนึ่งในจุดหมายปลายทางสุดฮิตสำหรับนักท่องเที่ยวชาวไทย คงหนีไม่พ้น กรุงโซล ประเทศเกาหลีใต้ แน่นอน... 
                        <a href="http://social.thaiairways.com/share-experience/international/seoul/KimLeehun/120213.html">READ MORE ></a>
                    </p>
                </div>
            </div>
            <?php
    }
}else{
    for($i = 1;$i <=12 ;$i++){    
            ?>
            <div class="lists col-md-4 col-sm-6">
                <div class="thumb focuspoint">
                    <img src="<?php echo base_url('assets/images/share/lists/thumb-1.jpg');?>" alt="" />
                </div>
                <div class="author">
                    <img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>lonelyseasion</b> <span class="date">21 Apr 14</span>
                </div>
                <div class="intro">
                    <h3>Dabble in Hongkong Part 1</h3>
                    <p>One of Osaka's wealthiest destinations, Aoyama 
                        is not the wildly experimental... <a href="<?php echo site_url('shareexperience/'.$type.'/'.$country.'/part'.$i.'.html');?>">READ MORE ></a></p>
                </div>
            </div>
            <?php 
    }
}
            ?>

            <?php /*?>
            <div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-1.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>lonelyseasion</b> <span class="date">21 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 1</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-2.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>pooklookphoto</b> <span class="date">20 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 2</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-3.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>tokyocs</b> <span class="date"> 16 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 3</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-1.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>lonelyseasion</b> <span class="date">21 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 1</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-2.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>pooklookphoto</b> <span class="date">20 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 2</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-3.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>tokyocs</b> <span class="date"> 16 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 3</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-1.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>lonelyseasion</b> <span class="date">21 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 1</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-2.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>pooklookphoto</b> <span class="date">20 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 2</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-3.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>tokyocs</b> <span class="date"> 16 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 3</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-1.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>lonelyseasion</b> <span class="date">21 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 1</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-2.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>pooklookphoto</b> <span class="date">20 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 2</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
<div class="lists col-md-4 col-sm-6">
<div class="thumb focuspoint">
<img src="<?php echo base_url('assets/images/share/lists/thumb-3.jpg');?>" alt="" />
</div>
<div class="author">
<img src="<?php echo base_url('assets/images/share/lists/thumb.jpg');?>" alt="" class="img-circle thumb"><b>tokyocs</b> <span class="date"> 16 Apr 14</span>
</div>
<div class="intro">
<h3>Dabble in Hongkong Part 3</h3>
<p>One of Osaka's wealthiest destinations, Aoyama 
is not the wildly experimental... <a href="<?php echo site_url('shareexperience/detail');?>">READ MORE ></a></p>
</div>
</div>
       <?php */?>
        </div>
    </div>
</div>