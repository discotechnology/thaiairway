<div class="row home-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-1.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/nexttrip_square.png');?>" alt="" class="img-responsive visible-xs" />
                <div class="top">
                    <span class="category">Share Experience</span>
                    <span class="tag">NEXT TRIP</span>
                </div>
                <div class="bottom">
                    <h1>Two Attractions in Changwon, Korea</h1>
                    <h3>Posted on :  August  13, 2014</h3>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-2.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/lastesttrip_square.png');?>" alt="" class="img-responsive visible-xs"/>
                <div class="top">
                    <span class="category">Share Experience</span>
                    <span class="tag">LAST TRIP</span>
                </div>
                <div class="bottom">
                    <h1>Two Attractions in Changwon, Korea</h1>
                    <h3>Posted on :  August  13, 2014</h3>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-3.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/highlight_square.jpg');?>" alt="" class="img-responsive visible-xs"/>
                <div class="top visible-xs">
                    <span class="tag hightlight">Highlight</span>
                </div>
                <div class="right">
                    <span class="tag hidden-xs">Highlight</span><span class="clearfix"></span>
                    <h1>Two Attractions in Changwon, Korea</h1>
                    <h3>Posted on :  August  13, 2014</h3>
                    <p class="hidden-sm hidden-xs">
                        On September 27, 2012. Thai Airways 
                        International Public Company Limited 
                        (THAI)
                    </p>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-4.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/general_square.png');?>" alt="" class="img-responsive visible-xs"/>
                <div class="top">
                    <span class="category">General Service</span>
                </div>
                <div class="bottom">
                    <h1>Pre-order meal 01-NOV-2013</h1>
                    <h3 class="hidden-sm">Posted on :  August  13, 2014</h3>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-5.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/nut_square.png');?>" alt="" class="img-responsive visible-xs"/>
                <div class="top">
                    <span class="category">General Service</span>
                </div>
                <div class="bottom">
                    <h1>Nut Allergy</h1>
                    <h3 class="hidden-sm">Posted on :  August  13, 2014</h3>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-6.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/sawasdee_square.png');?>" alt="" class="img-responsive visible-xs"/>
                <div class="top">

                </div>
                <div class="bottom">
                    <h1>SAWASDEE : June 2014</h1>
                    <h3 class="hidden-sm">Posted on :  August  13, 2014</h3>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-7.jpg');?>" alt="" class="img-responsive hidden-xs"/>
                <img src="<?php echo base_url('assets/images/home/dutyfree_square.png');?>" alt="" class="img-responsive visible-xs"/>
                <div class="top">
                </div>
                <div class="bottom">
                    <h1>Duty Free (THAI Airways)</h1>
                    <h3 class="hidden-sm">Posted on :  August  13, 2014</h3>
                    <p><a href="#" class="readmore">Read More...</a></p>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 box-content">
                <img src="<?php echo base_url('assets/images/home/content-8.jpg');?>" alt="" class="img-responsive"/>
            </div>
        </div>
    </div>
</div>

<div class="row footer-content">
    <div class="col-sm-12">
        <p align="center"><img src="<?php echo base_url('assets/images/footer/content.png');?>" alt="" class="img-responsive"/></p>
    </div>
</div>
