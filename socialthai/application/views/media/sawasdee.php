<div class="sawasdee-content">
    <div class="row royal">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="title">Royal Issues</h1>
                </div>
            </div>
            <div class="row royal-book">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <?php $data = $this->MediaModel->getRoyalData();?>
                        <?php foreach ($data as $value){?>
                        <div class="col-xs-6 col-sm-3 book">
                            <a href="<?php echo $value[1];?>" target="_blank">
                                <center><img src="<?php echo $value[0];?>" alt="" class="img-responsive"></center>
                            </a>
                        </div>
                        <? }?>
                        <?php /*?>
<div class="col-sm-3"><img src="<?php echo base_url('assets/images/media/sawasdee/royal/book-1.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-3"><img src="<?php echo base_url('assets/images/media/sawasdee/royal/book-2.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-3"><img src="<?php echo base_url('assets/images/media/sawasdee/royal/book-3.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-3"><img src="<?php echo base_url('assets/images/media/sawasdee/royal/book-4.png');?>" alt="" class="img-responsive"></div>
<?php */?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row fortnightly">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="title">Fortnightly Press</h1>
                </div>
            </div>
            <div class="row fortnightly-book">
                <?php $i=0;$data = $this->MediaModel->getFortnightlyData();?>
                <?php foreach ($data as $value){$i++;?>
                <div class="col-xs-6 col-sm-2 <?php echo ($i==1)?'col-sm-offset-1':'';?> book">
                    <a href="<?php echo $value[1]?>" target="_blank">
                        <center><img src="<?php echo $value[0];?>" alt="" class="img-responsive"></center>
                    </a>
                </div>
                <?php 
                                                if($i == 5)
                                                {
                                                    $i=0;
                ?>
            </div>
            <div class="row fortnightly-book">
                <?
                                                }
                ?>
                <?php }?>
            </div>
            <?php /*?>
<div class="row fortnightly-book">
<div class="col-sm-2 col-sm-offset-1"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-1.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-2.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-3.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-4.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-5.png');?>" alt="" class="img-responsive"></div>
</div>
<div class="row fortnightly-book">
<div class="col-sm-2 col-sm-offset-1"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-1.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-2.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-3.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-4.png');?>" alt="" class="img-responsive"></div>
<div class="col-sm-2"><img src="<?php echo base_url('assets/images/media/sawasdee/fortnightly/book-5.png');?>" alt="" class="img-responsive"></div>
</div>
<?php */?>
        </div>
    </div>
</div>