<div class="row entertainment-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h1 class="title">June 2014</h1>
                <div class="row">
                    <div class="col-md-8  col-md-offset-2">
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/zUo3Edc62mk"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>