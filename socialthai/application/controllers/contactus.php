<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('contactus/index',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
    public function socialteam()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('contactus/socialteam',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
    public function thaiworldwideoffice()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('contactus/thaiworldwideoffice',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
}