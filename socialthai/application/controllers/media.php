<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        show_404();
    }

    public function entertainment()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('media/entertainment',$dataContent,true);
        $this->load->view('masterpage',$data);
    }

    public function sawasdee()
    {
        $this->load->model('MediaModel');
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('media/sawasdee',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
    
    public function a380()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('media/a380',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
}
