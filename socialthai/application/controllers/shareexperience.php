<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shareexperience extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        show_error(404);
    }

    public function domestic(){
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('shareexperience/domestic',$dataContent,true);
        $this->load->view('masterpage',$data);
    }

    public function international(){
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('shareexperience/international',$dataContent,true);
        $this->load->view('masterpage',$data);
    }

    public function lists($country = ''){
        if($country == '')
        {
            show_error(404);
        }else{
            $data = array();
            $dataContent = array();
            $dataContent['country'] = $country;
            if($country == 'hongkong'){
                $dataContent['name'] = 'Hong Kong';
                $dataContent['type'] = 'international';
            }else if($country == 'madrid'){
                $dataContent['name'] = 'Madrid';
                $dataContent['type'] = 'international';
            }else if($country == 'seoul'){
                $dataContent['name'] = 'Seoul';
                $dataContent['type'] = 'international';
            }else if($country == 'tokyo'){
                $dataContent['name'] = 'Tokyo';
                $dataContent['type'] = 'international';
            }else if($country == 'shanghai'){
                $dataContent['name'] = 'Shanghai';
                $dataContent['type'] = 'international';
            }else if($country == 'hongkong'){
                $dataContent['name'] = 'Hong Kong';
                $dataContent['type'] = 'international';
            }else if($country == 'chiangmai'){
                $dataContent['name'] = 'Chiang Mai';
                $dataContent['type'] = 'domestic';
            }else if($country == 'phuket'){
                $dataContent['name'] = 'Phuket';
                $dataContent['type'] = 'domestic';
            }else if($country == 'bangkok'){
                $dataContent['name'] = 'Bangkok';
                $dataContent['type'] = 'domestic';
            }else if($country == 'buriram'){
                $dataContent['name'] = 'Buriram';
                $dataContent['type'] = 'domestic';
            }
            $data['content'] = $this->load->view('shareexperience/lists',$dataContent,true);
            $this->load->view('masterpage',$data);
        }
    }

    public function detail($country = '', $article = ''){
        $data = array();
        $dataContent = array();
        $dataContent['country'] = $country;
        $dataContent['article'] = $article;
        if($country == 'hongkong'){
            $dataContent['name'] = 'Hong Kong';
            $dataContent['type'] = 'international';
        }else if($country == 'madrid'){
            $dataContent['name'] = 'Madrid';
            $dataContent['type'] = 'international';
        }else if($country == 'seoul'){
            $dataContent['name'] = 'Seoul';
            $dataContent['type'] = 'international';
        }else if($country == 'tokyo'){
            $dataContent['name'] = 'Tokyo';
            $dataContent['type'] = 'international';
        }else if($country == 'shanghai'){
            $dataContent['name'] = 'Shanghai';
            $dataContent['type'] = 'international';
        }else if($country == 'hongkong'){
            $dataContent['name'] = 'Hong Kong';
            $dataContent['type'] = 'international';
        }else if($country == 'chiangmai'){
            $dataContent['name'] = 'Chiang Mai';
            $dataContent['type'] = 'domestic';
        }else if($country == 'phuket'){
            $dataContent['name'] = 'Phuket';
            $dataContent['type'] = 'domestic';
        }else if($country == 'bangkok'){
            $dataContent['name'] = 'Bangkok';
            $dataContent['type'] = 'domestic';
        }else if($country == 'buriram'){
            $dataContent['name'] = 'Buriram';
            $dataContent['type'] = 'domestic';
        }
        $data['content'] = $this->load->view('shareexperience/detail',$dataContent,true);
        $this->load->view('masterpage',$data);

    }
}
