<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }
  public function index()
  {
    $data = array();
    $dataContent = array();
    $data['content'] = $this->load->view('home',$dataContent,true);
      $data['seoTitle'] = "Home";
    $this->load->view('masterpage',$data);
  }
}
