<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterpage extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }
  public function index()
  {
    $data = array();
    $dataContent = array();
    $data['content'] = '';
    $this->load->view('masterpage',$data);
  }
}
