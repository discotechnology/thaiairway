<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beforeyourfly extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('beforeyourfly/index',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
    
    public function detail()
    {
        $data = array();
        $dataContent = array();
        $data['content'] = $this->load->view('beforeyourfly/detail',$dataContent,true);
        $this->load->view('masterpage',$data);
    }
}
