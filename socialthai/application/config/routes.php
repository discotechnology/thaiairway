<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

//Home
$route['home.html'] = "home/index";

//Beforeyoufly
$route['beforeyourfly.html'] = "beforeyourfly/index";
$route['beforeyourfly/(:any)/(:any).html'] = "beforeyourfly/detail/$1/$2";

//Shareexperience
$route['shareexperience/domestic/(:any)/(:any).html'] = "shareexperience/detail/$1/$2";
$route['shareexperience/international/(:any)/(:any).html'] = "shareexperience/detail/$1/$2";

$route['shareexperience/domestic.html'] = "shareexperience/domestic";
$route['shareexperience/domestic/(:any).html'] = "shareexperience/lists/$1";

$route['shareexperience/international.html'] = "shareexperience/international";
$route['shareexperience/international/(:any).html'] = "shareexperience/lists/$1";


//Media
$route['media/entertainment.html'] = "media/entertainment";
$route['media/sawasdee.html'] = "media/sawasdee";
$route['media/a380.html'] = "media/a380";

//Contact Us
$route['contactus/socialteam.html'] = "contactus/socialteam";
$route['contactus/thaiworldwideoffice.html'] = "contactus/thaiworldwideoffice";

/* End of file routes.php */
/* Location: ./application/config/routes.php */