$(function(){
    $capture = $('.beforeyourfly-content .caption');
    $capture.hide();
    $('.beforeyourfly-content .before-group').hover(hoverIn,hoverOut);
    function hoverIn(){
        $(this).find( ".caption" ).slideDown();
    }
    function hoverOut(){
        $(this).find( ".caption" ).slideUp();
    }
});